package com.amazecode.mapper;

import com.amazecode.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/17 21:48
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
