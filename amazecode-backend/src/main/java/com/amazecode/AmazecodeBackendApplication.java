package com.amazecode;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 启动类
 * @author AmazeCode
 * @date 2023/2/19 12:01
 * @version 1.0
 */
@SpringBootApplication
@MapperScan("com.amazecode.mapper")
public class AmazecodeBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmazecodeBackendApplication.class, args);
    }

}
