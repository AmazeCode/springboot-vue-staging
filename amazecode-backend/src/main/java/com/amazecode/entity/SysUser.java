package com.amazecode.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/17 21:31
 */
@Data
@TableName("sys_user")
public class SysUser {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String name;

    private Integer age;

    /**
     *   <p>
     *      如果期望日期格式不是yyyy-MM-dd hh:mm:ss类型,可以使用 @JsonFormat进行日期格式自定义
     *       ，@JsonFormat优先级比较高，会覆盖全局设置
     *       @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
     *       @JsonFormat(pattern = "hh:mm:ss", timezone = "GMT+8")
     *   </p>
     */
    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
