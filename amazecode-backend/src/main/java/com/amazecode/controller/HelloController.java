package com.amazecode.controller;

import com.amazecode.common.result.Result;
import com.amazecode.entity.SysUser;
import com.amazecode.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/17 21:46
 */
@RestController
@RequestMapping("/api")
public class HelloController {

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("/hello")
    public Object hello() {
        List<SysUser> list = sysUserService.list();
        return Result.success(list);
    }

    @GetMapping("/failure")
    public Object failure(){
        return Result.failed("测试自定义失败信息");
    }

    //测试字符串的包装
    @GetMapping("/string")
    /** @ResponseNotWrap 请求需要返回原始值时添加 **/
    public Object string(){
        return "测试字符串包装";
    }

    /**
     * <p> 验证空指针异常捕获 </p>
     *
     * @return: boolean
     * @author AmazeCode
     * @date: 2023/2/19 17:50
     */
    @GetMapping("/nullpoint")
    public boolean nullPoint() {
        String str = null;
        str.equals("111");
        return true;
    }

    /**
     * <p> 验证其他异常捕获 </p>
     *
     * @return: boolean
     * @author AmazeCode
     * @date: 2023/2/19 17:50
     */
    @GetMapping("/byZero")
    public boolean byZero() {
        int i = 1/0;
        return true;
    }
}
