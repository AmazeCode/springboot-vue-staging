package com.amazecode.common.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/19 15:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {

    private Integer code;

    private String message;

    private T data;

    public static Result<Void> success() {
        Result<Void> result = new Result<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMessage(ResultCode.SUCCESS.getMessage());
        return result;
    }

    /**
     * <p> 成功,有返回数据 </p>
     *
     * @param data
     * @return: com.amazecode.common.result.Result<T>
     * @author AmazeCode
     * @date: 2023/2/19 15:48
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(ResultCode.SUCCESS.getCode(),ResultCode.SUCCESS.getMessage(),data);
    }

    /**
     * <p> 成功,有返回数据 </p>
     *
     * @param data
     * @param message
     * @return: com.amazecode.common.result.Result<T>
     * @author AmazeCode
     * @date: 2023/2/19 15:49
     */
    public static <T> Result<T> success(T data, String message) {
        return new Result<>(ResultCode.SUCCESS.getCode(),message,data);
    }

    public static Result<?> failed() {
        return new Result<>(ResultCode.FAILURE.getCode(), ResultCode.FAILURE.getMessage(), null);
    }

    public static Result<?> failed(String message) {
        return new Result<>(ResultCode.FAILURE.getCode(), message, null);
    }

    public static Result<?> failed(Integer code, String message) {
        return new Result<>(code, message, null);
    }

    public static <T> Result<T> instance(Integer code, String message, T data) {
        Result<T> result = new Result<>();
        result.setCode(code);
        result.setMessage(message);
        result.setData(data);
        return result;
    }
}
