package com.amazecode.common.result;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 返回枚举
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/19 15:38
 */
@Getter
@AllArgsConstructor
public enum ResultCode {

    //成功提示码
    SUCCESS(20000, "成功"),

    //自定义失败信息
    FAILURE(50000, "失败"),

    //通用错误码 50001~50099
    PROGRAM_INSIDE_EXCEPTION(50001, "程序内部异常"),
    REQUEST_PARAM_ERROR(50002, "请求参数错误"),
    NULL_POINT_ERROR(50003,"空指针异常！"),
    BIZ_OTHER_ERROR(50004,"其他异常！");

    //用户模块错误码 50100~50199
    //商品模块错误码 50200~50299
    //订单模块错误码 50300~50399

    private final Integer code;
    private final String message;
}
