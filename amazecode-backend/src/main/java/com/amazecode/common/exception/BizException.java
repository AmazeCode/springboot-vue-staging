package com.amazecode.common.exception;

/**
 * 自定义异常类
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/19 17:16
 */
public class BizException extends RuntimeException {

    /**
     * 状态码
     */
    private Integer code;

    /**
     * <p> </p>
     *
     * @param code 状态码
     * @return:
     * @author AmazeCode
     * @date: 2023/2/19 15:04
     */
    public BizException(int code) {
        this.code = code;
    }

    /**
     * <p> </p>
     *
     * @param code 状态码
     * @param message 消息内容
     * @return:
     * @author AmazeCode
     * @date: 2023/2/19 15:04
     */
    public BizException(int code,String message) {
        super(message);
        this.code = code;
    }

    /**
     * <p> cause清楚的定位到是哪里的错（异常的起源）</p>
     *
     * @param code 状态码
     * @param message 消息内容
     * @param cause 异常起源
     * @return:
     * @author AmazeCode
     * @date: 2023/2/19 15:02
     */
    public BizException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    /**
     * <p> </p>
     *
     * @param code 状态码
     * @param cause  异常起源
     * @return:
     * @author AmazeCode
     * @date: 2023/2/19 15:03
     */
    public BizException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
