package com.amazecode.common.annotation;

import java.lang.annotation.*;

/**
 * 返回放行注解,在类和方法上使用此注解，返回原生值
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/19 16:03
 */
@Target({ElementType.TYPE,ElementType.METHOD}) // 在方法和类上使用上使用
@Retention(RetentionPolicy.RUNTIME) // 运行时
@Documented
public @interface ResponseNotWrap {
}
