package com.amazecode.service;

import com.amazecode.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/17 21:49
 */
public interface SysUserService extends IService<SysUser> {
}
