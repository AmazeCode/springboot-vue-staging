package com.amazecode.service.impl;

import com.amazecode.entity.SysUser;
import com.amazecode.mapper.SysUserMapper;
import com.amazecode.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author AmazeCode
 * @version 1.0
 * @date 2023/2/17 21:51
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
}
